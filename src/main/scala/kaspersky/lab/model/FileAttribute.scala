package kaspersky.lab.model

import kaspersky.lab.routes.AttributeName._

case class FileAttribute(
  md5: String,
  zone: Option[Short] = None,
  format: Option[Short] = None,
  size: Option[Long] = None
) {
  def getFieldValue: PartialFunction[AttributeName, Option[AnyVal]] = {
    case Zone => zone
    case Format => format
    case Size => size
  }
}
