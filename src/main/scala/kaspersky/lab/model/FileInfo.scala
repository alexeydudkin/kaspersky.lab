package kaspersky.lab.model

case class FileInfo(
  id: Long,
  md5: Option[String] = None,
  zone: Option[Short] = None,
  format: Option[Short] = None,
  size: Option[Long] = None
)
