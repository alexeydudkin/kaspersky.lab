package kaspersky.lab.bl

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.routing.FromConfig
import com.softwaremill.macwire.wire
import kaspersky.lab.di.ApplicationModule
import kaspersky.lab.messages.{GetFileAttributes, GetMd5, UploadFileAttributeRequest, UploadFileInfoRequest}

object RequestManager {
  val props = Props(wire[RequestManager])
}

class RequestManager extends Actor with ApplicationModule {

  override def system: ActorSystem = context.system

  val insertWorker: ActorRef = context.actorOf(FromConfig.props(Props(wire[InsertWorker])), "insertWorker")
  val readWorker: ActorRef = context.actorOf(FromConfig.props(Props(wire[ReadWorker])), "readWorker")

  override def receive: Receive = {
    case request: UploadFileInfoRequest =>
      insertWorker forward request

    case request: UploadFileAttributeRequest =>
      insertWorker forward request

    case request: GetFileAttributes =>
      readWorker forward request

    case request: GetMd5 =>
      readWorker forward request

  }
}
