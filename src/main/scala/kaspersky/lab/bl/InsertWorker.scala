package kaspersky.lab.bl

import akka.actor.Actor
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.messages.{FileUploadResponse, UploadFileAttributeRequest, UploadFileInfoRequest}
import kaspersky.lab.model.{FileAttribute, FileInfo}
import kaspersky.lab.services.{FileAttributeDAOService, FileInfoDAOService}
import scala.concurrent.Future
import scala.util.control.NonFatal
import akka.pattern.pipe

class InsertWorker(
  fileInfoDAOService: FileInfoDAOService,
  fileAttributeDAOService: FileAttributeDAOService
) extends Actor with LazyLogging {

  implicit val ec = context.dispatcher

  override def receive: Receive = {
    case UploadFileInfoRequest(id, md5, zone) =>
      (for {
          _ <- fileInfoDAOService.insert(FileInfo(id, Some(md5), Some(zone)))
          _ <- fileAttributeDAOService.insert(FileAttribute(md5, Some(zone)))
        } yield FileUploadResponse(id)
        ).recoverWith(fallback) pipeTo sender

    case UploadFileAttributeRequest(id, format, size) =>
      fileInfoDAOService
        .insert(FileInfo(id, format = Some(format), size = Some(size)))
        .flatMap {
          case info: FileInfo if info.md5.isDefined =>
            fileAttributeDAOService
              .insert(FileAttribute(info.md5.get, info.zone, info.format, info.size))
              .map(_ => FileUploadResponse(id))
          case _ =>
            Future.successful(FileUploadResponse(id))
        }.recoverWith(fallback) pipeTo sender()
  }

  private def fallback[T]: PartialFunction[Throwable, Future[T]] = {
    case NonFatal(error) =>
      logger.error("An error occur during data insertion", error)
      Future.failed(error)
  }
}
