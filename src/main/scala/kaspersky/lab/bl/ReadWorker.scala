package kaspersky.lab.bl

import akka.actor.Actor
import akka.pattern.pipe
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.messages.{AttributeResponse, GetFileAttributes, GetMd5, Md5Response}
import kaspersky.lab.services.FileAttributeDAOService

class ReadWorker(
  fileAttributeDAOService: FileAttributeDAOService
) extends Actor with LazyLogging {

  implicit val ec = context.dispatcher

  override def receive: Receive = {
    case GetFileAttributes(md5) =>
      fileAttributeDAOService
        .read(md5)
        .map(AttributeResponse) pipeTo sender

    case GetMd5(attributes) =>
      fileAttributeDAOService
        .getMd5By(attributes)
        .map(attrs => Md5Response(attrs.map(_.md5))) pipeTo sender
  }

}
