package kaspersky.lab.utils

import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.routes.{AttributeType, CollectionAttribute, RangeAttribute, SingleAttribute}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Try}
import scala.util.control.NonFatal

trait ApplicationMarshalling extends LazyLogging {

  implicit object attributeTypeUnmarshaller extends Unmarshaller[String, AttributeType] {
    override def apply(value: String)(implicit ec: ExecutionContext, materializer: Materializer): Future[AttributeType] =
      Try(
        SingleAttribute(value.toLong)
      ).orElse(
        Try {
          value.split("-").toList match {
            case from :: to :: Nil => RangeAttribute(from.toLong, to.toLong)
          }
        }).orElse(
        Try {
          CollectionAttribute(value.split(",").toList.map(_.toLong))
        }
      ).recoverWith {
        case NonFatal(error) =>
          logger.error("An error occur during unmarshalling", error)
          Failure(error)
      }.fold(Future.failed, Future.successful)
  }

}

object ApplicationMarshalling extends ApplicationMarshalling
