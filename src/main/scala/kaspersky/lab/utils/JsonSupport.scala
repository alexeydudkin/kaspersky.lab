package kaspersky.lab.utils

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import kaspersky.lab.messages._
import kaspersky.lab.model.FileAttribute
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val fileInfoRequestF = jsonFormat3(UploadFileInfoRequest)
  implicit val fileAttributeRequestF = jsonFormat3(UploadFileAttributeRequest)
  implicit val fileUploadResponseF = jsonFormat1(FileUploadResponse)
  implicit val fileAttributeF = jsonFormat4(FileAttribute)
  implicit val attributeResponseF = jsonFormat1(AttributeResponse)
  implicit val md5ResponseF = jsonFormat1(Md5Response)
}

object JsonSupport extends JsonSupport
