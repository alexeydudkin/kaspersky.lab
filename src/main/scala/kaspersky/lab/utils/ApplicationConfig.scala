package kaspersky.lab.utils

import com.typesafe.config.{Config, ConfigFactory}

object ApplicationConfig {
  val conf: Config = ConfigFactory.load().getConfig("kaspersky-server")

  val host: String = conf.getString("http.host")
  val port: Int = conf.getInt("http.port")
}
