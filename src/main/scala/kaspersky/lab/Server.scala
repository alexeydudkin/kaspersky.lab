package kaspersky.lab

import akka.Done
import akka.actor.{ActorSystem, CoordinatedShutdown}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.bl.RequestManager
import kaspersky.lab.di.RouteModule
import kaspersky.lab.utils.ApplicationConfig

import scala.concurrent.{ExecutionContext, Future}

object Server extends App with RouteModule with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("HttpServer")

  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val executionContext: ExecutionContext = system.dispatcher

  private val httpHost = ApplicationConfig.host
  private val httpPort = ApplicationConfig.port

  logger.debug("Start binding HTTP server")

  val requestManager =
    system
    .actorOf(RequestManager.props, "requestManager")

  private val serverBindingFuture: Future[ServerBinding] =
    Http().bindAndHandle(routes.map(_.routes).reduce(_ ~ _), httpHost, httpPort)

  serverBindingFuture.foreach(_ =>
    logger.info("HTTP server is bound to {}:{}", httpHost, httpPort))

  private val shutdown = CoordinatedShutdown(system)

  shutdown.addTask(
    CoordinatedShutdown.PhaseBeforeServiceUnbind,
    "log-shutdown-started") { () =>
      Future.successful {
        logger.warn("Application shutdown is triggered.")
        Done
      }
    }

  shutdown.addTask(
    CoordinatedShutdown.PhaseServiceUnbind,
    "shutdown-connection-pool") { () =>
      serverBindingFuture.flatMap(_.unbind).flatMap { _ =>
        Http().shutdownAllConnectionPools
      }.map { _ =>
        logger.debug("Shutdown of the connection pool is finished")
        Done
      }
    }
}
