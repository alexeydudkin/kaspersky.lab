package kaspersky.lab.routes

import kaspersky.lab.routes.AttributeName.AttributeName

sealed trait AttributeType
case class SingleAttribute(value: Long) extends AttributeType
case class CollectionAttribute(values: List[Long]) extends AttributeType
case class RangeAttribute(from: Long, to: Long) extends AttributeType

object AttributeName extends Enumeration {
  type AttributeName = Value

  val Zone, Format, Size = Value
}

case class QueryAttribute(name: AttributeName, attributeType: AttributeType)
