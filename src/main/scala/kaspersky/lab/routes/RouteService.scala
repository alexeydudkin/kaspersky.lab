package kaspersky.lab.routes

import akka.http.scaladsl.server.Route
import kaspersky.lab.utils.JsonSupport

trait RouteService extends JsonSupport {
  def routes: Route
}
