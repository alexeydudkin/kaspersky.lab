package kaspersky.lab.routes

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.ParameterDirectives.ParamMagnet
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.messages._
import kaspersky.lab.utils.ApplicationMarshalling._
import akka.pattern.ask
import akka.util.Timeout
import scala.reflect._
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import AttributeName._
import akka.http.scaladsl.marshalling.ToEntityMarshaller

class ApplicationRoute(manager: ActorRef) extends RouteService with LazyLogging {
  implicit val timeout = Timeout(5.seconds)

  override def routes: Route =
    post {
      path("info") {
        entity(as[UploadFileInfoRequest]) { request =>
          completeRequest[FileUploadResponse](request)
        }
      } ~
        path("attributes") {
          entity(as[UploadFileAttributeRequest]) { request =>
            completeRequest[FileUploadResponse](request)
          }
        }
    } ~
      get {
        (path("md5") & parameters(attributeParameters)) { case (zone, format, size) =>
          val attributes = List(
            QueryAttribute(Zone, zone),
            QueryAttribute(Format, format),
            QueryAttribute(Size, size)
          )
          completeRequest[Md5Response](GetMd5(attributes))
        } ~
          (path("attributes") & parameter('md5)) { case md5 =>
            completeRequest[AttributeResponse](GetFileAttributes(md5))
          }
      }

  private val attributeParameters = ParamMagnet(
    'zone.as[AttributeType],
    'format.as[AttributeType],
    'size.as[AttributeType]
  )

  private def completeRequest[T](request: RestRequest)(implicit m: ToEntityMarshaller[T], tag: ClassTag[T]) =
    onComplete((manager ? request).mapTo[T]) {
      case Success(result) => complete(StatusCodes.OK, result)
      case Failure(error) =>
        logger.error("An error occur", error)
        complete(StatusCodes.InternalServerError)
    }
}
