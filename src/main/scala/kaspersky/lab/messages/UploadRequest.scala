package kaspersky.lab.messages

import kaspersky.lab.model.FileAttribute
import kaspersky.lab.routes.QueryAttribute

trait RestRequest

trait UploadRequest extends RestRequest{
  def id: Long
}

case class UploadFileInfoRequest(
  id: Long,
  md5: String,
  zone: Short
) extends UploadRequest

case class UploadFileAttributeRequest(
  id: Long,
  format: Short,
  size: Long
) extends UploadRequest

case class GetFileAttributes(md5: String) extends RestRequest
case class GetMd5(attributes: List[QueryAttribute]) extends RestRequest

case class FileUploadResponse(id: Long)
case class AttributeResponse(attributes: Option[FileAttribute])
case class Md5Response(md5: Seq[String])
