package kaspersky.lab.di

import akka.actor.ActorSystem
import com.softwaremill.macwire._
import kaspersky.lab.services.{FileAttributeDAOService, FileInfoDAOService, StubFileAttributeDAOServiceImpl, StubFileInfoDAOServiceImpl}

trait ApplicationModule {
  def system: ActorSystem

  lazy val fileInfoDAOService: FileInfoDAOService = wire[StubFileInfoDAOServiceImpl]
  lazy val fileAttributeDAOService: FileAttributeDAOService = wire[StubFileAttributeDAOServiceImpl]
}
