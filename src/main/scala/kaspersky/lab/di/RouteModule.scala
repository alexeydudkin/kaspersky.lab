package kaspersky.lab.di

import akka.actor.ActorRef
import kaspersky.lab.routes.{ApplicationRoute, RouteService}
import com.softwaremill.macwire._

trait RouteModule {
  def requestManager: ActorRef

  lazy val applicationRoute: RouteService = wire[ApplicationRoute]
  lazy val routes: Set[RouteService] = wireSet[RouteService]
}
