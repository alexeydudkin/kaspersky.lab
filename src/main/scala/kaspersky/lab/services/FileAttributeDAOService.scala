package kaspersky.lab.services

import akka.actor.ActorSystem
import akka.util.Timeout
import kaspersky.lab.model.FileAttribute

import scala.concurrent.duration._
import scala.concurrent.Future
import akka.pattern.ask
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.routes.{CollectionAttribute, QueryAttribute, RangeAttribute, SingleAttribute}

import scala.collection.immutable.NumericRange

trait FileAttributeDAOService {
  def insert(fileAttribute: FileAttribute): Future[String]

  def read(md5: String): Future[Option[FileAttribute]]

  def getMd5By(query: List[QueryAttribute]): Future[Seq[FileAttribute]]
}

class StubFileAttributeDAOServiceImpl(system: ActorSystem) extends FileAttributeDAOService with LazyLogging {
  import AttributeQueryService._

  implicit val ec = system.dispatchers.lookup("database-dispatcher")
  implicit val timeout = Timeout(5.seconds)

  private val stubDatabase = system.actorOf(StubDatabase.fileAttributeDBProps)

  override def insert(fileAttribute: FileAttribute): Future[String] =
    (stubDatabase ? InsertFileAttribute(fileAttribute)).mapTo[PersistedMd5Is].map(_.md5)

  override def read(md5: String): Future[Option[FileAttribute]] =
    (stubDatabase ? ReadFileAttribute(md5)).mapTo[FileAttributeIs].map(_.maybeFileAttribute)

  override def getMd5By(query: List[QueryAttribute]): Future[Seq[FileAttribute]] =
    (stubDatabase ? ReadAllAttributes)
      .mapTo[AllAttributesIs]
      .map(_.attributes.filter(attributeFilterFunc(query)))

  private def attributeFilterFunc(query: List[QueryAttribute]): FileAttribute => Boolean =
    attribute =>
      query
        .map(filterConditionBy(attribute).apply)
        .map(_.getOrElse(false)).reduce(_ && _)
}

object AttributeQueryService extends LazyLogging {
  def filterConditionBy(attribute: FileAttribute): PartialFunction[QueryAttribute, Option[Boolean]] = {
    case QueryAttribute(name, SingleAttribute(value)) =>
      attribute.getFieldValue.lift(name).flatten.map(_ == value)

    case QueryAttribute(name, CollectionAttribute(values)) =>
      attribute.getFieldValue.lift(name).flatten.map(values.contains)

    case QueryAttribute(name, RangeAttribute(from, to)) =>
      val range = NumericRange.inclusive[Long](from, to, 1)
      attribute.getFieldValue.lift(name).flatten.map {
        case value: Long => range.contains(value)
        case value: Short => range.contains(value.toLong)
        case value =>
          logger.error("Unsupported attribute field type `{}`", value)
          false
      }

    case queryAttribute =>
      logger.error("Unsupported query attribute `{}`", queryAttribute)
      Some(false)
  }
}
