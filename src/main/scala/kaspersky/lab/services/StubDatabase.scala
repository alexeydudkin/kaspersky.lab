package kaspersky.lab.services

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.LazyLogging
import kaspersky.lab.model.{FileAttribute, FileInfo}

object StubDatabase {
  val fileInfoDBProps = Props[StubFileInfoDatabase]
  val fileAttributeDBProps = Props[StubFileAttributeDatabase]
}

class StubFileInfoDatabase extends Actor with LazyLogging {
  override def receive: Receive = processing()

  private def processing(idToFileInfo: Map[Long, FileInfo] = Map.empty): Receive = {
    case InsertFileInfo(fileInfo) =>
      logger.debug("Receive file info (id: {}) insert msg", fileInfo.id)
      val updated =
        idToFileInfo
          .get(fileInfo.id)
          .map(info => info.copy(
            md5 = info.md5 orElse fileInfo.md5,
            zone = info.zone orElse fileInfo.zone,
            format = info.format orElse fileInfo.format,
            size = info.size orElse fileInfo.size
          )).getOrElse(fileInfo)

      context.become(processing(idToFileInfo + (fileInfo.id -> updated)))
      sender ! PersistedIdIs(updated)

    case ReadFileInfo(id) =>
      sender ! FileInfoIs(idToFileInfo.get(id))
  }
}

class StubFileAttributeDatabase extends Actor with LazyLogging {

  override def receive: Receive = processing()

  private def processing(md5ToFileAttribute: Map[String, FileAttribute] = Map.empty): Receive = {
    case InsertFileAttribute(fileAttribute) =>
      logger.debug("Receive file attribute (md5: {}) insert msg", fileAttribute.md5)
      val updated =
        md5ToFileAttribute
          .get(fileAttribute.md5)
          .map(attr => attr.copy(
            zone = attr.zone orElse fileAttribute.zone,
            format = attr.format orElse fileAttribute.format,
            size = attr.size orElse fileAttribute.size
          )).getOrElse(fileAttribute)

      context.become(processing(md5ToFileAttribute + (fileAttribute.md5 -> updated)))
      sender ! PersistedMd5Is(fileAttribute.md5)

    case ReadFileAttribute(md5) =>
      sender ! FileAttributeIs(md5ToFileAttribute.get(md5))

    case ReadAllAttributes =>
      sender ! AllAttributesIs(md5ToFileAttribute.values.toSeq)
  }
}

case class InsertFileInfo(fileInfo: FileInfo)

case class ReadFileInfo(id: Long)

case class InsertFileAttribute(fileAttribute: FileAttribute)

case class ReadFileAttribute(md5: String)

case object ReadAllAttributes

case class PersistedIdIs(fileInfo: FileInfo)

case class FileInfoIs(maybeFileInfo: Option[FileInfo])

case class PersistedMd5Is(md5: String)

case class FileAttributeIs(maybeFileAttribute: Option[FileAttribute])

case class AllAttributesIs(attributes: Seq[FileAttribute])
