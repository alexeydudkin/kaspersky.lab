package kaspersky.lab.services

import akka.actor.ActorSystem
import kaspersky.lab.model.FileInfo
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Future
import scala.concurrent.duration._

trait FileInfoDAOService {
  def insert(fileInfo: FileInfo): Future[FileInfo]

  def read(id: Long): Future[Option[FileInfo]]
}

class StubFileInfoDAOServiceImpl(system: ActorSystem) extends FileInfoDAOService {
  implicit val ec = system.dispatchers.lookup("database-dispatcher")
  implicit val timeout = Timeout(5.seconds)

  private val stubDatabase = system.actorOf(StubDatabase.fileInfoDBProps)

  override def insert(fileInfo: FileInfo): Future[FileInfo] =
    (stubDatabase ? InsertFileInfo(fileInfo)).mapTo[PersistedIdIs].map(_.fileInfo)

  override def read(id: Long): Future[Option[FileInfo]] =
    (stubDatabase ? ReadFileInfo(id)).mapTo[FileInfoIs].map(_.maybeFileInfo)
}
