package kaspersky.lab

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import kaspersky.lab.bl.{InsertWorker, ReadWorker}
import kaspersky.lab.di.ApplicationModule
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import com.softwaremill.macwire.wire
import kaspersky.lab.messages._
import kaspersky.lab.model.FileAttribute

class ApplicationActorSpec extends TestKit(ActorSystem("Application-Spec"))
  with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender with ApplicationModule with TestData {

  val insertWorker: ActorRef = system.actorOf(Props(wire[InsertWorker]))
  val readWorker: ActorRef = system.actorOf(Props(wire[ReadWorker]))

  "Application actors" must {
    "store incoming data" in {
      insertWorker ! UploadFileInfoRequest(ID.id_1, MD5.md5_1, ZONE.zone_1)
      insertWorker ! UploadFileAttributeRequest(ID.id_1, FORMAT.format_1, SIZE.size_1)
      expectMsgAllOf(
        FileUploadResponse(ID.id_1),
        FileUploadResponse(ID.id_1)
      )
    }

    "return file attributes by md5 value" in {
      readWorker ! GetFileAttributes(MD5.md5_1)
      expectMsgAllOf(
        AttributeResponse(Some(FileAttribute(MD5.md5_1, Some(ZONE.zone_1), Some(FORMAT.format_1), Some(SIZE.size_1))))
      )
    }

    "return file md5 file by attribute list" in {
      insertWorker ! UploadFileInfoRequest(ID.id_2, MD5.md5_2, ZONE.zone_2)
      insertWorker ! UploadFileAttributeRequest(ID.id_2, FORMAT.format_2, SIZE.size_2)

      insertWorker ! UploadFileAttributeRequest(ID.id_3, FORMAT.format_3, SIZE.size_3)
      insertWorker ! UploadFileInfoRequest(ID.id_3, MD5.md5_3, ZONE.zone_1)

      readWorker ! GetMd5(QUERY.query_1)
      readWorker ! GetMd5(QUERY.query_2)
      readWorker ! GetMd5(QUERY.query_3)

      expectMsgAllOf(
        FileUploadResponse(ID.id_2),
        FileUploadResponse(ID.id_2),
        FileUploadResponse(ID.id_3),
        FileUploadResponse(ID.id_3),
        Md5Response(Stream(MD5.md5_1)),
        Md5Response(Stream(MD5.md5_2)),
        Md5Response(Stream(MD5.md5_1, MD5.md5_2))
      )
    }
  }

  override protected def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
}
