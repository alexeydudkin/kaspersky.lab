package kaspersky.lab

import kaspersky.lab.routes.{CollectionAttribute, QueryAttribute, RangeAttribute, SingleAttribute}
import kaspersky.lab.services.AttributeQueryService
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import kaspersky.lab.routes.AttributeName._

class ApplicationSpec extends WordSpecLike with Matchers with BeforeAndAfterAll with TestData {
  "Application" must {
    "correct working with single attribute query" in {
      AttributeQueryService
        .filterConditionBy(ATTRIBUTE.attribute_1)
        .lift(QueryAttribute(Zone, SingleAttribute(ZONE.zone_1)))
        .flatten shouldBe Some(true)
    }

    "correct working with collection attribute query" in {
      AttributeQueryService
      .filterConditionBy(ATTRIBUTE.attribute_2)
        .lift(QueryAttribute(Format, CollectionAttribute(List(FORMAT.format_1, FORMAT.format_2))))
        .flatten shouldBe Some(true)
    }

    "correct working with range attribute query including boundary values" in {
      AttributeQueryService
        .filterConditionBy(ATTRIBUTE.attribute_3)
        .lift(QueryAttribute(Size, RangeAttribute(0, 1500)))
        .flatten shouldBe Some(false)

      AttributeQueryService
        .filterConditionBy(ATTRIBUTE.attribute_2)
        .lift(QueryAttribute(Size, RangeAttribute(0, 1024)))
        .flatten shouldBe Some(true)

      AttributeQueryService
        .filterConditionBy(ATTRIBUTE.attribute_2)
        .lift(QueryAttribute(Size, RangeAttribute(1024, 1500)))
        .flatten shouldBe Some(true)
    }
  }
}
