package kaspersky.lab

import kaspersky.lab.model.FileAttribute
import kaspersky.lab.routes.{CollectionAttribute, QueryAttribute, RangeAttribute, SingleAttribute}
import kaspersky.lab.routes.AttributeName._

trait TestData {
  object ID {
    val id_1: Long = 1
    val id_2: Long = 2
    val id_3: Long = 3
  }

  object MD5 {
    val md5_1 = "5a105e8b9d40e1329780d62ea2265d8a"
    val md5_2 = "dcb7d6dfa46fcce69457e708578d5"
    val md5_3 = "75278a84557a9b23ec93e1f8af80b568"
  }

  object ZONE {
    val zone_1: Short = 1
    val zone_2: Short = 2
  }

  object FORMAT {
    val format_1: Short = 100
    val format_2: Short = 200
    val format_3: Short = 300
  }

  object SIZE {
    val size_1: Long = 512
    val size_2: Long = 1024
    val size_3: Long = 2048
  }

  object ATTRIBUTE {
    val attribute_1 =  FileAttribute(MD5.md5_1, Some(ZONE.zone_1), Some(FORMAT.format_1), Some(SIZE.size_1))
    val attribute_2 =  FileAttribute(MD5.md5_2, Some(ZONE.zone_2), Some(FORMAT.format_2), Some(SIZE.size_2))
    val attribute_3 =  FileAttribute(MD5.md5_3, Some(ZONE.zone_1), Some(FORMAT.format_3), Some(SIZE.size_3))
  }

  object QUERY {
    val query_1 = List(
      QueryAttribute(Zone, SingleAttribute(1)),
      QueryAttribute(Format, CollectionAttribute(List(100, 200))),
      QueryAttribute(Size, RangeAttribute(512, 1500))
    )

    val query_2 = List(
      QueryAttribute(Zone, RangeAttribute(1, 3)),
      QueryAttribute(Format, SingleAttribute(200)),
      QueryAttribute(Size, CollectionAttribute(List(512, 1024)))
    )

    val query_3 = List(
      QueryAttribute(Zone, CollectionAttribute(List(1, 2))),
      QueryAttribute(Format, RangeAttribute(100, 300)),
      QueryAttribute(Size, RangeAttribute(512, 1024))
    )
  }
}
