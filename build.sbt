lazy val akkaHttpVersion = "10.1.5"
lazy val akkaVersion = "2.5.16"
lazy val slf4jVersion = "1.7.25"
lazy val logbackVersion = "1.2.3"
lazy val scalaTestVersion = "3.0.5"
lazy val scalaLoggingVersion = "3.9.0"
lazy val macwireVersion = "2.3.1"

lazy val root = (project in file("."))
  .enablePlugins(DockerPlugin)
  .enablePlugins(JavaServerAppPackaging)
  .settings(
    inThisBuild(Seq(
      organization := "kaspersky.lab",
      scalaVersion := "2.12.4",
      fork in run := true
    )),

    name := "Kaspersky Lab Test Case",
    version := "1.0.0-SNAPSHOT",
    maintainer := "Kaspersky Lab",

    dockerBaseImage := "openjdk:8-slim",

    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "org.slf4j" % "slf4j-api" % slf4jVersion,
      "ch.qos.logback" % "logback-classic" % logbackVersion,
      "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion,
      "com.softwaremill.macwire" %% "macros" % macwireVersion % Provided,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % scalaTestVersion % Test
    )
  )
