**Все мысли по поводу оптимизации и улучшения прототипа приведу здесь.**

Даже не знаю, что здесь можно расписать, т.к. это всего лишь прототип, и конечно его можно улучшать "вдоль и поперек".

Сборка осуществляется в контейнер, можно брать и разворачивать в том же openshifte с настройкой оркестрации и пр.

Для получения данных о файлах я использовал HTTP API. Одним из вариантов - было использование Message Brokers, 
с адаптацией текущего решения под новый формат входных данных.

Взамен мокирования БД, я конечно бы предпочел полноценное эластичное хранилище, со всеми ее возможностями
(работа при масштабировании сервера). Поиск md5 по значению аттрибутов, можно тоже организовать с помощью БД,
если ключами таблицы будут выступать входные параметры. Но здесь возможно больше подойдет тот же Spark для 
быстрой обработки.

Нужно посмотреть в сторону ES + CQRS + за счет того же Akka Sharding размазывать это все дело по нодам.
При этом получаем возможность активирования\пассивирования шардов, т.е. управления загрузкой машин.

